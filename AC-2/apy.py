from flask import Flask, jsonify, request
import sqlite3
import threading

app = Flask(__name__)

class App:
    @app.route('/livros', methods=['GET'])
    def consulta_books():
        db = sqlite3.connect('banco_de_livros.db')
        cursor = db.execute("SELECT * FROM db_livros")
        livro = [dict(numero_livro=row[0], autor=row[1], titulo=row[2]) for row in cursor.fetchall()]
        return jsonify({'Livro cadastrados': livro})    
        db.close()
        

    @app.route('/livros', methods=['POST'])
    def add_book():
        db = sqlite3.connect('banco_de_livros.db')    
        num_livro = request.json['numero_livro']
        autor = request.json['autor']
        titulo = request.json['titulo']

        db.execute("INSERT INTO db_livros (numero_livro, autor, titulo) VALUES (?, ?, ?)", (num_livro, autor, titulo))
        db.commit()
        db.close()
        return jsonify({'message': 'Livro adicionado com sucesso'})

    @app.route('/livros/<int:numero_livro>', methods=['DELETE'])
    def delete_book(numero_livro):
        print(numero_livro)
        db = sqlite3.connect('banco_de_livros.db')
        cursor = db.cursor()
        cursor.execute("DELETE FROM db_livros WHERE numero_livro = ?", (numero_livro,))
        db.commit()
        db.close()
        return jsonify({'message': 'Livro deletado com sucesso.'})


        
if __name__ == "__main__":
    app.run(port=200,host='localhost',debug=True)
