from app import app
import unittest
import json

class AppTest(unittest.TestCase):
    def setUp(self):
        self.client = app.test_client()

    def teste_get_api_livro(self):
        response = app.test_client().get('/livros')
        self.assertEqual(response.status_code, 200)

    def test_post_api_livro(self):
        payload = {'numero_livro': 46, 'titulo': 'Mortal com cambalhota', 'autor': 'Carol'}
        headers = {'Content-Type': 'application/json'}
        response = app.test_client().post('/livros', headers=headers, data=json.dumps(payload))
        self.assertEqual(response.status_code, 200)

    def test_delete_livro(self):
        response = self.client.delete('/livros/46')
        self.assertEqual(response.status_code, 200)

if __name__ == "__main__":
    unittest.main()